<?php

function milion_widgets_init()
{
	register_sidebar(array(
		'name' => __('Widget Area', '1000000'),
		'id' => 'widget-area',
		'description' => __('The widget area', '1000000'),
		'before_widget' => '<aside id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
}

add_action('widgets_init', 'milion_widgets_init');

add_filter( 'the_category', 'replace_cat_tag' );
 
function replace_cat_tag($text) {
	$text = str_replace('rel="category tag"', "", $text); return $text;
}

if ( ! function_exists( 'milion_comment' ) ) :
function milion_comment( $comment, $args, $depth )
{
	$GLOBALS['comment'] = $comment;

?>
<div>
<?php

	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>

	<section class="commment post pingback">
		<p>
			<?php _e('Pingback:', '1000000'); ?>
			<?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', '1000000' ), ' ' ); ?>
		</p>
	</section>

		<?php
		break;
		default:
		?>

				<section id="comment-<?php comment_ID(); ?>" class="comment">
						<div class="comment-meta">
							<?php printf( __( '%s @', '1000000' ), get_comment_author_link() ); ?>
							<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
							<?php /* translators: 1: date, 2: time */
								printf( __( '%1$s o %2$s', '1000000' ), get_comment_date(), get_comment_time() ); ?></a>:
							<?php edit_comment_link( __( '(Edit)', '1000000' ), ' ' ); ?>
						</div><!-- .comment-meta .commentmetadata -->

						<?php if ( $comment->comment_approved == '0' ) : ?>
							<em><?php _e( 'Your comment is awaiting moderation.', '1000000' ); ?></em>
							<br>
						<?php endif; ?>
 
					<div class="comment-content"><?php echo get_avatar( $comment, 40 ); ?> <?php comment_text(); ?></div>
				</section><!-- #comment-## -->
 
			<?php
			break;
		endswitch;
	}
	endif;

?>
