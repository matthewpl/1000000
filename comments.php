<?php // Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die (__('Please do not load this page directly. Thanks!','1000000'));
	if ( post_password_required() ) { ?>
  <p class="nocomments"><?php _e('Ten post jest chroniony hasłem. Wpisz je aby móc zobaczyć komentarze.','1000000') ?></p>
	<?php
		return;
	}
?>

<!-- You can start editing here. -->
<?php if (have_comments()) : ?>
<h3 id="comments-title"><?php comments_number(__('Brak komentarzy','1000000'), __('Komentarze:','1000000'), __('Komentarze:','1000000'));?></h3>
	<nav>
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</nav>

	<?php wp_list_comments('style=div&callback=milion_comment'); ?>

	<nav>
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</nav>
 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ('open' == $post->comment_status) : ?>
		<!-- If comments are open, but there are no comments. -->

	<?php else : // comments are closed ?>
		<?php _e('Brak możliwości komentowania.','1000000') ?></p> -->

	<?php endif; ?>
<?php endif; ?>

<?php if ('open' == $post->comment_status) : ?>

<div id="respond">

   <h3>Dodaj komentarz:</h3>

<div class="cancel-comment-reply">
	<small><?php cancel_comment_reply_link(); ?></small>
</div>

<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p><?php _e('Musisz być zalogowany aby napisać komentarz.','1000000')?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php _e('Zaloguj się.','1000000') ?></p>
<?php else : ?>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

<?php if ( $user_ID ) : ?>

<p><?php _e('Zalogowany jako','1000000') ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Wyloguj się.','1000000') ?>"><?php _e('Wyloguj się.','1000000') ?> &raquo;</a></p>

<?php else : ?>
<p><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" <?php if ($req) echo "aria-required='true'"; ?> />
<label for="author"><?php _e('Nick','1000000')?> <?php if ($req) echo "(".__('wymagane','1000000').")"; ?></label></p>
<p><input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" <?php if ($req) echo "aria-required='true'"; ?> />
<label for="email"><?php _e('e-mail','1000000') ?> <?php if ($req) echo "(".__('wymagany','1000000').")"; ?></label></p>
<p><input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="22" />
<label for="url"><?php _e('www','1000000') ?></label></p>
<?php endif; ?>

<p><label for="comment" class="skip"><?php _e('Treść komentarza','1000000') ?>:</label></p>
<p><textarea name="comment" id="comment" cols="100" rows="10"></textarea></p>
<p><input name="submit" type="submit" id="submit" value="<?php _e('Wyślij komentarz','1000000') ?>" />
<?php comment_id_fields(); ?>
</p>
<div><?php do_action('comment_form', $post->ID); ?></div>
</form>

<?php endif; // If registration required and not logged in ?>
</div>

<?php endif; // if you delete this the sky will fall on your head ?>