	<?php get_header(); ?>

	<section class="main center debug">
		<nav class="sidebar padding debug">
			<?php get_sidebar(); ?>
		</nav>

		<section class="content padding debug">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<article class="post" id="post-<?php the_ID(); ?>">
				<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
				<section class="meta">
					<?php the_author(); ?> @ <?php the_time(_c('Y-m-d|Dates','100000')) ?>
				</section>

				<?php the_content(); ?>
			</article>

			<?php endwhile; else: ?>

			<p>
				<?php _e('Sorry, no posts matched your criteria.'); ?>
			</p>

			<?php endif; ?>
		</section>
	</section>

	<?php get_footer(); ?>
