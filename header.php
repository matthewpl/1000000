<!doctype html>
<html lang="pl">
	<head>
		<title><?php
		global $page, $paged;

		wp_title('-', true, 'right');

		bloginfo('name');

		?></title>

		<meta charset="<?php bloginfo( 'charset' ); ?>" />

		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RDF/RSS 1.0 feed" href="<?php bloginfo('rdf_url'); ?>" />
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS 0.92 feed" href="<?php bloginfo('rss_url'); ?>" />
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS 2.0 feed" href="<?php bloginfo('rss2_url'); ?>" />
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Atom feed" href="<?php bloginfo('atom_url'); ?>" />

		<?php wp_head(); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-26411837-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
	</head>

	<body>
		<header class="center debug">
			<nav class="menu debug">
				<?php wp_nav_menu(array('theme_location'=>'primary')); ?>
			</nav>
			<section class="logo debug">
				<h1 class="padding" id="title"><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
<!--				<h2 id="description"><?php bloginfo('description'); ?></h2> -->
			</section>
		</header>
