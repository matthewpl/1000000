	<?php get_header(); ?>

	<section class="main center debug">
		<nav class="sidebar padding debug">
			<?php get_sidebar(); ?>
		</nav>

		<section class="content padding debug">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<article class="post" id="post-<?php the_ID(); ?>">
				<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
				<section class="meta">
					<?php the_author(); ?> @ <?php the_time(_c('Y-m-d|Dates','100000')); echo ' — '; _e('Categories'); ?>: <?php the_category(', '); ?>
				</section>

				<?php the_content(__('More...')); ?>
			</article>

			<?php
				global $wp_query;
				if ($wp_query->current_post + 1 < $wp_query->post_count)
				{
					echo '<hr>';
				}?>

			<?php endwhile; else: ?>

			<p>
				<?php _e('Sorry, no posts matched your criteria.'); ?>
			</p>

			<?php endif; ?>

			<?php posts_nav_link(' &#8212; ', __('&laquo; Previous Page'), __('Next Page &raquo;')); ?>
		</section>
	</section>

	<?php get_footer(); ?>
